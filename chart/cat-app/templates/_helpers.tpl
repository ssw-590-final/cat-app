{{/* vim: set filetype=mustache: */}}
{{- define "app.labels" -}}
owner: {{ .Values.global.owner }}
environment: {{ .Values.global.env }}
app: {{ .Release.Name }}
{{- end -}}