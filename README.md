# cat-app
[![push](https://github.com/ssw-590/cat-app/actions/workflows/push.yaml/badge.svg)](https://github.com/ssw-590/cat-app/actions/workflows/push.yaml)

Version: `1.1.12`

A Flask application that displays images of cats!

## Repo Structure
```
.
├── README.md
├── .github
│   └── workflows
│       ├── build.yaml
│       ├── push.yaml
│       └── test.yaml
├── app/*
├── chart/cat-app/*
├── Containerfile
├── requirements.txt
└── version.md
```

## Workflows
### Build

When a commit is pushed to non-default branches, the workflow runs a test to ensure the [`Containerfile`](./Containerfile) image builds successfully.

When a commit is pushed to the default branch (`main`), the workflow runs a docker build test of the [`Containerfile`](./Containerfile) image. On success, the workflow uses [`bumpversion`](https://pypi.org/project/bumpversion/) to upgrade the version of the application. See the [`.bumpversion.cfg`](.bumpversion.cfg) for details on where the version is defined/updated. Bumpversion commits this change to the default branch and then creates a new tag.

### Push

When bumpversion creates a new tag, the workflow builds the [`Containerfile`](./Containerfile) image and publishes it to DockerHub.