"""app.main"""
from fastapi import FastAPI
from fastapi.responses import FileResponse
import random

app = FastAPI()


@app.get("/")
async def main():
    return {
        "message": "Hello, World!",
        "test": "test123"
    }

@app.get("/ready")
async def main():
    return {
        "message": "I'm ready!",
        "status": "ready"
    }

@app.get("/cat")
async def main():
    random_num = random.randint(1, 5)
    return FileResponse(f"/src/app/photos/cat-{random_num}.jpeg")